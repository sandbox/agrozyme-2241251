<?php

trait SingletonPattern {

  final protected function __construct() {

  }

  static function getInstance() {
    static $cache = array();
    $class = get_called_class();

    if (FALSE == isset($cache[$class])) {
      $cache[$class] = new $class();
    }

    return $cache[$class];
  }
}

class ClassHook {

  use SingletonPattern;

  protected $module = '';

  static function hasTraits($class, $trait) {
    $traits = static::getTraits($class);
    return isset($traits[$trait]);
  }

  static function getTraits($class = NULL) {
    if (is_null($class)) {
      $class = get_called_class();
    }

    static $cache = array();

    if (isset($cache[$class])) {
      return $cache[$class];
    }

    $traits = class_uses($class);
    $cache[$class] = (empty($traits)) ? array() : $traits;
    return $cache[$class];
  }

  static function loadHook(array $files) {
    $module = static::getInstance()->module;
    $base_class = get_called_class();
    $class = '';

    foreach ($files as $file) {
      module_load_include('inc', $module, $file);
    }

    while ($class = static::upperCamel(array_pop($files))) {
      if (is_subclass_of($class, $base_class)) {
        return $class::getInstance();
      }
    }

    return static::getInstance();
  }

  static function getHook($type = '', $bundle = '') {
    static $cache = array();
    $module = static::getInstance()->module;

    if (isset($cache[$module][$type][$bundle])) {
      return $cache[$module][$type][$bundle];
    }

    $files = array(
      $module . '_' . $type,
      $module . '_' . $type . '_' . $bundle,
    );

    $cache[$module][$type][$bundle] = static::loadHook($files);
    return $cache[$module][$type][$bundle];
  }

  static function upperCamel($text) {
    return str_replace(' ', '', ucwords(str_replace('_', ' ', $text)));
  }

  static function lowerCamel($text) {
    return lcfirst(static::upperCamel($text));
  }

  static function hookMethodName($id, $hook = '') {
    return 'do' . static::upperCamel($id . ' ' . $hook);
  }

  static protected function breakWorkflow($url = '/', $message = '') {
    form_set_error('', filter_xss($message));
    drupal_goto($url);
  }

  static protected function getHookInfo() {
    static $cache = array();
    $module = static::getInstance()->module;

    if (empty($module)) {
      return array();
    }

    if (isset($cache[$module])) {
      return $cache[$module];
    }

    $info = array();

    foreach (static::getTraits() as $trait) {
      $info += static::getTraitHookInfo($trait);
    }

    $class = get_called_class();

    foreach ($info as $index => $item) {
      if (is_callable(array($class, $item))) {
        $cache[$module][$module . '_' . $index] = $item;
      }
    }

    return $cache[$module];
  }

  static protected function getTraitHookInfo($trait) {
    $info = array();
    $method = lcfirst($trait) . 'Info';

    if (FALSE == is_callable(array(get_called_class(), $method))) {
      return array();
    }

    try {
      return static::$method();
    } catch (Exception $exception) {
      return array();
    }
  }

  static protected function getDefaultParameterName(array $invoke) {
    $data = array_combine($invoke, $invoke);
    $prefix = '$_';
    $count = 0;
    $index = $prefix . $count;

    while (isset($data[$index])) {
      $count++;
      $index = $prefix . $count;
    }

    return $index;
  }

  static protected function buildHookFunction($hook, $method) {
    $default = &drupal_static('ClassHookDefaultParameters', array());
    $class = get_called_class();
    $called = array();
    $invoke = array();
    $code = array();
    $reflection = new ReflectionMethod($class, $method);
    $default[$hook] = array();

    foreach ($reflection->getParameters() as $item) {
      $result = static::buildParameter($hook, $item);
      $called[] = $result['called'];
      $invoke[] = $result['invoke'];
    }

    $default_name = static::getDefaultParameterName($invoke);

    $code[] = "function $hook (" . implode(', ', $called) . ') {';

    if (FALSE == empty($default[$hook])) {
      $code[] = "$default_name = &drupal_static('ClassHookDefaultParameters', array());";
    }

    foreach (array_keys($default[$hook]) as $name) {
      $code[] = "$name = is_null($name) ? $default_name" . "[__FUNCTION__]['$name'] : $name;";
    }

    $code[] = $class . '::' . $method . '(' . implode(', ', $invoke) . ');';
    $code[] = '}';
    return implode("\n", $code);
  }

  static protected function buildParameter($hook, ReflectionParameter $item) {
    $result = array();
    $default = &drupal_static('ClassHookDefaultParameters', array());
    $name = '$' . $item->getName();
    $result['invoke'] = $name;

    if ($item->isDefaultValueAvailable()) {
      $default[$hook][$name] = $item->getDefaultValue();
      $name .= ' = NULL';
    }

    if ($item->isPassedByReference()) {
      $name = '&' . $name;
    }

    $result['called'] = $name;
    return $result;
  }

  static function registerClassHook() {
    $class = get_called_class();
    $code = array('<?php');

    foreach (static::getHookInfo() as $hook => $method) {
      if (function_exists($hook)) {
        continue;
      }

      $code[] = static::buildHookFunction($hook, $method);
    }

    php_eval(implode("\n", $code));
  }
}

trait FormHook {

  static function formAlter(&$form, &$form_state, $form_id) {
    if (FALSE == is_subclass_of(get_called_class(), 'ClassHook')) {
      return;
    }

    if (isset($form['#user']) && isset($form['#user_category'])) {
      $type = 'user';
      $bundle = $form['#user_category'];
    }
    elseif (isset($form['#entity_type']) && isset($form['#bundle'])) {
      $type = $form['#entity_type'];
      $bundle = $form['#bundle'];
    }
    else {
      $type = '';
      $bundle = '';
    }

    $hook = static::getHook($type, $bundle);
    $hook->doFormAlter($form, $form_state, $form_id);
    $method = 'form' . static::upperCamel($form_id) . 'Alter';

    if (is_callable(array($hook, $method))) {
      return $hook->$method($form, $form_state, $form_id);
    }
  }

  static function widgetFormAlter(&$element, &$form_state, $context) {

  }

  static protected function formHookInfo() {
    return array(
      'form_alter' => 'formAlter',
      'field_widget_form_alter' => 'widgetFormAlter',
    );
  }

  protected function hideField(&$element) {
    $element['#type'] = 'value';
    $element['#required'] = FALSE;
  }

  protected function doFieldAlter(& $form, & $form_state, $form_id) {
    foreach ($this->getFieldAlterInfo($form_id, 'alter') as $item) {
      $method = $item . 'FieldAlter';

      if (is_callable(array($this, $method))) {
        $this->$method($form, $form_state, $form_id);
      }
    }
  }

  protected function doFieldHidden(& $form, & $form_state, $form_id) {
    foreach ($this->getFieldAlterInfo($form_id, 'hidden') as $item) {
      if (isset($form[$item])) {
        $this->hideField($form[$item]);
      }
    }
  }

  protected function doFieldUnset(& $form, & $form_state, $form_id) {
    foreach ($this->getFieldAlterInfo($form_id, 'unset') as $item) {
      unset($form[$item]);
    }
  }

  protected function fieldAlterInfo() {
    return array();
  }

  protected function getFieldAlterInfo($form_id = NULL, $hook = NULL) {
    static $cache = array();

    if (empty($cache)) {
      $cache = $this->fieldAlterInfo();
    }

    if (is_null($form_id)) {
      return $cache;
    }

    if (empty($cache[$form_id])) {
      return array();
    }

    if (is_null($hook)) {
      return $cache[$form_id];
    }

    return empty($cache[$form_id][$hook]) ? array() : $cache[$form_id][$hook];
  }

  protected function doFormAlter(&$form, &$form_state, $form_id) {
    $info = $this->getFieldAlterInfo($form_id);

    if (empty($info) || (FALSE == is_array($info))) {
      return;
    }

    foreach (array_keys($info) as $hook) {
      if (empty($info[$hook])) {
        continue;
      }

      $method = 'doField' . static::upperCamel($hook);

      if (is_callable(array($this, $method))) {
        $this->$method($form, $form_state, $form_id);
      }
    }
  }

  protected function doWidgetFormAlter(&$element, &$form_state, $context) {
    dpm(func_get_args());
//    $info = $this->getFieldAlterInfo($form_id);
//
//    if (empty($info) || (FALSE == is_array($info))) {
//      return;
//    }
//
//    foreach (array_keys($info) as $hook) {
//      if (empty($info[$hook])) {
//        continue;
//      }
//
//      $method = 'doField' . static::upperCamel($hook);
//
//      if (is_callable(array($this, $method))) {
//        $this->$method($form, $form_state, $form_id);
//      }
//    }
  }
}

trait EntityHook {

  static protected function entityCall($method, array &$args = array()) {
    if (FALSE == is_subclass_of(get_called_class(), 'ClassHook')) {
      return;
    }

    list($entity, $type) = $args;

    if (is_array($entity)) {
      $entity = reset($entity);
    }

    $boundle = empty($entity->type) ? '' : $entity->type;
    $hook = static::getHook($type, $boundle);
    $callback = array(static::getHook($type, $boundle), static::hookMethodName($method));

    if (is_callable($callback)) {
      return call_user_func_array($callback, $args);
    }
  }

  static function entityLoad($entities, $type) {
    $args = array(&$entities, &$type);
    return static::entityCall(__FUNCTION__, $args);
  }

  static function entityPrepareView($entities, $type, $langcode) {
    $args = array(&$entities, &$type, &$langcode);
    return static::entityCall(__FUNCTION__, $args);
  }

  static function entityPresave($entity, $type) {
    $args = array(&$entity, &$type);
    return static::entityCall(__FUNCTION__, $args);
  }

  static function entityDelete($entity, $type) {
    $args = array(&$entity, &$type);
    return static::entityCall(__FUNCTION__, $args);
  }

  static function entityView($entity, $type, $view_mode, $langcode) {
    $args = array(&$entity, &$type, &$view_mode, &$langcode);
    return static::entityCall(__FUNCTION__, $args);
  }

  static function entityViewAlter(& $build, $type) {
    $args = array(&$build, &$type);
    return static::entityCall(__FUNCTION__, $args);
  }

  static protected function entityHookInfo() {
    return array(
      'entity_load' => 'entityLoad',
      'entity_prepare_view' => 'entityPrepareView',
      'entity_presave' => 'entityPresave',
      'entity_delete' => 'entityDelete',
      'entity_view' => 'entityView',
      'entity_view_alter' => 'entityViewAlter',
    );
  }

  protected function doEntityLoad($entities, $type) {

  }

  protected function doEntityPrepareView($entities, $type, $langcode) {

  }

  protected function doEntityPresave($entity, $type) {

  }

  protected function doEntityDelete($entity, $type) {

  }

  protected function doEntityView($entity, $type, $view_mode, $langcode) {

  }

  protected function doEntityViewAlter(& $build, $type) {

  }
}

trait CommerceProductHook {

  static function CommerceProductCanDelete($entity) {
    if (FALSE == is_subclass_of(get_called_class(), 'ClassHook')) {
      return TRUE;
    }

    $type = 'commerce_product';
    $hook = static::getHook($type, static::boundle($entity));
    return $hook->doCommerceProductCanDelete($entity, $type);
  }

  static protected function commerceProductHookInfo() {
    return array(
      'commerce_product_can_delete' => 'commerceProductCanDelete',
    );
  }

  protected function doCommerceProductCanDelete($product) {
    return TRUE;
  }
}